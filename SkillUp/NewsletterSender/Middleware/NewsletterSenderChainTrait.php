<?php

namespace SkillUp\NewsletterSender\Middleware;

use SkillUp\NewsletterSender\Manager\NewsletterSenderManager;
use SkillUp\NewsletterSender\Senders\NewsletterSenderBase;

trait NewsletterSenderChainTrait
{
	private NewsletterSenderManager $next;

	/**
	 * This method can be used to build a chain of sender objects.
	 *
	 * @param NewsletterSenderBase $next
	 *
	 * @return NewsletterSenderChainInterface
	 */
	public function addNextSender(NewsletterSenderBase $next): NewsletterSenderChainInterface
	{
		$this->next = new NewsletterSenderManager($next);

		return $next;
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function startSendingNext(array $userData): bool
	{
		if (!isset($this->next) || !$this->next) {
			return true;
		}

		return $this->next->startSending($userData);
	}
}