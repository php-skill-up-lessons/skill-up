<?php

namespace SkillUp\NewsletterSender\Middleware;

use SkillUp\NewsletterSender\Senders\NewsletterSenderBase;

interface NewsletterSenderChainInterface
{
	public function addNextSender(NewsletterSenderBase $next): NewsletterSenderChainInterface;

	public function startSendingNext(array $userData): bool;
}