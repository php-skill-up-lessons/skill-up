<?php

namespace SkillUp\NewsletterSender;

interface NewsletterSenderInterface
{
	public function isExistSendParams(array $userData): bool;

	public function emptySendParams(array $userData): void;

	public function isValidSendParams(array $userData): bool;

	public function alreadySentMessage(string $senderId): void;

	public function send(array $userData): bool;
}