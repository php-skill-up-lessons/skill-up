<?php

namespace SkillUp\NewsletterSender\Senders;

use SkillUp\NewsletterSender\Middleware\NewsletterSenderChainTrait;

class NewsletterSenderPush extends NewsletterSenderBase
{
	use NewsletterSenderChainTrait;

	public function __construct()
	{
		$this->userSenderField = 'device_id';
	}

	public function isValidSendParams(array $userData): bool
	{
		if (!preg_match('/^\w+$/u', $userData[$this->userSenderField])) {
			$this->notificator->error("Push device is not valid");

			return false;
		}

		return true;
	}

	public function alreadySentMessage(string $senderId): void
	{
		$this->notificator->warning("Push notification has already been sent with device_id '{$senderId}'");
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function send(array $userData): bool
	{
		$this->notificator->success("Push notification has been sent to user {$userData['name']} with device_id '{$userData[$this->userSenderField]}'");

		return true;
	}
}