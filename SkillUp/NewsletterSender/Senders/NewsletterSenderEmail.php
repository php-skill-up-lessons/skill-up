<?php

namespace SkillUp\NewsletterSender\Senders;

use SkillUp\NewsletterSender\Middleware\NewsletterSenderChainTrait;

class NewsletterSenderEmail extends NewsletterSenderBase
{
	use NewsletterSenderChainTrait;

	public function __construct()
	{
		$this->userSenderField = 'email';
	}

	public function isValidSendParams(array $userData): bool
	{
		if (!filter_var($userData[$this->userSenderField], FILTER_VALIDATE_EMAIL)) {
			$this->notificator->error("Email is not valid");

			return false;
		}

		return true;
	}

	public function alreadySentMessage(string $senderId): void
	{
		$this->notificator->warning("Email '{$senderId}' has already been sent");
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function send(array $userData): bool
	{
		$this->notificator->success("Email '{$userData[$this->userSenderField]}' has been sent to user {$userData['name']}");

		return true;
	}
}