<?php

namespace SkillUp\NewsletterSender\Senders;

use SkillUp\NewsletterSender\NewsletterSenderInterface;
use SkillUp\NewsletterSender\Middleware\NewsletterSenderChainInterface;
use SkillUp\Notifications\NotificationsInterface;

abstract class NewsletterSenderBase implements NewsletterSenderInterface, NewsletterSenderChainInterface
{
	protected NotificationsInterface $notificator;

	public string $userSenderField;

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function isExistSendParams(array $userData): bool
	{
		return !empty($userData[$this->userSenderField]);
	}

	/**
	 * @param array $userData
	 */
	public function emptySendParams(array $userData): void
	{
		$this->notificator->notice("User {$userData['name']} have empty Sender field '{$this->userSenderField}'");
	}

	public function alreadySentMessage(string $senderId): void
	{
		$this->notificator->warning("Message by '{$senderId}' has already been sent");
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function isCorrectUserName(array $userData): bool
	{
		return !empty($userData['name']);
	}

	public function setNotificator(NotificationsInterface $notificator): void
	{
		$this->notificator = $notificator;
	}
}