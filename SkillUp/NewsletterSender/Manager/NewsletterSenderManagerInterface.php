<?php

namespace SkillUp\NewsletterSender\Manager;

use SkillUp\NewsletterSender\Senders\NewsletterSenderBase;

interface NewsletterSenderManagerInterface
{
	public function __construct(NewsletterSenderBase $newsletterSender);

	/**
	 * Check params before send
	 *
	 * @return bool
	 */
	public function isCorrectParams(): bool;

	/**
	 * Check is been send by userSendId
	 *
	 * @param string $userSendId
	 *
	 * @return bool
	 */
	public function isSent(string $userSendId): bool;

	/**
	 * Start sending to user by $newsletterSender
	 *
	 * @param array $user
	 *
	 * @return bool
	 */
	public function startSending(array $user): bool;

	/**
	 * Saving sent userSendIds
	 *
	 * @param string $userSendId
	 */
	public function saveSendId(string $userSendId): void;
}