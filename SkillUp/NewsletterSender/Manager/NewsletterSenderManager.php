<?php

namespace SkillUp\NewsletterSender\Manager;

use SkillUp\NewsletterSender\Middleware\NewsletterSenderChainInterface;
use SkillUp\NewsletterSender\Middleware\NewsletterSenderChainTrait;
use SkillUp\NewsletterSender\Senders\NewsletterSenderBase;
use SkillUp\Notifications\NotificationsHtmlTable;

class NewsletterSenderManager implements NewsletterSenderManagerInterface, NewsletterSenderChainInterface
{
	use NewsletterSenderChainTrait;

	public NewsletterSenderBase $newsletterSender;
	public array $user;

	private NewsletterSenderManager $next;
	private array $userSentIds;

	public function __construct(NewsletterSenderBase $newsletterSender)
	{
		$newsletterSender->setNotificator(new NotificationsHtmlTable());
		$this->newsletterSender = $newsletterSender;
	}

	public function isCorrectParams(): bool
	{
		if (!$this->newsletterSender->isExistSendParams($this->user)) {
			$this->newsletterSender->emptySendParams($this->user);

			return false;
		}

		return $this->newsletterSender->isValidSendParams($this->user);
	}

	public function isSent(string $userSendId): bool
	{
		if (!isset($this->userSentIds[$this->newsletterSender->userSenderField])) {
			$this->userSentIds[$this->newsletterSender->userSenderField] = [];

			return false;
		}

		return in_array($userSendId, $this->userSentIds[$this->newsletterSender->userSenderField]);
	}

	public function saveSendId(string $userSendId): void
	{
		$this->userSentIds[$this->newsletterSender->userSenderField][] = $userSendId;
	}

	public function startSending(array $user): bool
	{
		$this->user = $user;

		if ($this->isCorrectParams()) {
			$userSendId = $this->user[$this->newsletterSender->userSenderField];

			if ($this->isSent($userSendId)) {
				$this->newsletterSender->alreadySentMessage($userSendId);

				return $this->startSendingNext($this->user);
			}

			if ($this->newsletterSender->send($this->user)) {
				$this->saveSendId($userSendId);
			}
		}

		return $this->startSendingNext($this->user);
	}
}