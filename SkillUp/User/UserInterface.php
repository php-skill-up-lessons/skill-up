<?php

namespace SkillUp\User;

interface UserInterface
{
	public static function getUsers(): array;
}