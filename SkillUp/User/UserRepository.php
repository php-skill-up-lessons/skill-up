<?php

namespace SkillUp\User;

class UserRepository implements UserInterface
{
	public static function getUsers(): array
	{
		return [
			[
				'name' => 'Ivan',
				'email' => 'ivan@test.com',
				'device_id' => 'Ks[dqweer4'
			],
			[
				'name' => 'Peter',
				'email' => 'peter@test.com'
			],
			[
				'name' => 'Mark',
				'device_id' => 'Ks[dqweer4'
			],
			[
				'name' => 'Nina',
				'email' => '...'
			],
			[
				'name' => 'Luke',
				'device_id' => 'vfehlfg43g'
			],
			[
				'name' => 'Zerg',
				'device_id' => ''
			],
			[
				'email' => '...',
				'device_id' => ''
			],
			[
				'name' => 'Peter2',
				'email' => 'peter@test.com'
			],
			[
				'name' => 'Luke2',
				'device_id' => 'vfehlfg43g'
			],
			[
				'name' => '',
				'email' => 'ivanovw@test.com',
				'device_id' => 'gv754vqnfv'
			],
		];
	}
}