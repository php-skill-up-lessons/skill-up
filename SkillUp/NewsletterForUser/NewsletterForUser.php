<?php

namespace SkillUp\NewsletterForUser;

use SkillUp\NewsletterSender\Manager\NewsletterSenderManager;
use SkillUp\NewsletterSender\Senders\NewsletterSenderBase;
use SkillUp\Notifications\NotificationsHtmlTable;
use SkillUp\User\UserInterface;

class NewsletterForUser implements NewsletterForUserInterface
{
	private UserInterface $users;
	private NewsletterSenderManager $senders;

	public function __construct(UserInterface $user, NewsletterSenderBase ...$newsletterSenders)
	{
		$this->users = $user;
		$this->setSenders(...$newsletterSenders);
	}

	public function setSenders(NewsletterSenderBase ...$newsletterSenders): void
	{
		$initSenderBase = array_shift($newsletterSenders);
		$this->senders = new NewsletterSenderManager($initSenderBase);

		foreach ($newsletterSenders as $newsletterSender) {
			$this->senders->addNextSender($newsletterSender);
		}
	}

	public function isCorrectMainParams(array $user): bool
	{
		return $this->senders->newsletterSender->isCorrectUserName($user);
	}

	public function startSend()
	{
		$users = $this->users->getUsers();
		$notificator = new NotificationsHtmlTable();

		foreach ($users as $number => $user) {
			if (!$this->isCorrectMainParams($user)) {
				$notificator->warning('&emsp;User Name is wrong');

				continue;
			} else {
				$notificator->info("User {$user['name']}...");
			}

			$this->senders->startSending($user);
		}
	}

}