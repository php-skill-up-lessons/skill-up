<?php

namespace SkillUp\NewsletterForUser;

use SkillUp\NewsletterSender\Senders\NewsletterSenderBase;
use SkillUp\User\UserInterface;

interface NewsletterForUserInterface
{
	public function __construct(UserInterface $user, NewsletterSenderBase ...$newsletterSenders);

	public function setSenders(NewsletterSenderBase ...$newsletterSenders): void;

	public function isCorrectMainParams(array $user): bool;

	public function startSend();
}