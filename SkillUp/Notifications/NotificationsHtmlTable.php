<?php

namespace SkillUp\Notifications;

class NotificationsHtmlTable implements NotificationsInterface
{
	const NOTIFICATION_TYPE_MESSAGE = 1;
	const NOTIFICATION_TYPE_SUCCESS = 2;
	const NOTIFICATION_TYPE_INFO = 3;
	const NOTIFICATION_TYPE_NOTICE = 4;
	const NOTIFICATION_TYPE_WARNING = 5;
	const NOTIFICATION_TYPE_ERROR = 6;

	private array $classes = [
		self::NOTIFICATION_TYPE_MESSAGE => 'grey',
		self::NOTIFICATION_TYPE_SUCCESS => 'green',
		self::NOTIFICATION_TYPE_INFO => 'blue',
		self::NOTIFICATION_TYPE_NOTICE => 'orange',
		self::NOTIFICATION_TYPE_WARNING => 'brown',
		self::NOTIFICATION_TYPE_ERROR => 'red',
	];

	public function message(string $text)
	{
		$this->viewInTag($text, self::NOTIFICATION_TYPE_MESSAGE);
	}

	public function success(string $text)
	{
		$this->viewInTag($text, self::NOTIFICATION_TYPE_SUCCESS, '&#9786;');
	}

	public function info(string $text)
	{
		$this->viewInTag('&emsp;' . $text, self::NOTIFICATION_TYPE_INFO, '&#8505;');
	}

	public function notice(string $text)
	{
		$this->viewInTag($text . ' !', self::NOTIFICATION_TYPE_NOTICE, '&#9757;');
	}

	public function warning(string $text)
	{
		$this->viewInTag($text . ' !!', self::NOTIFICATION_TYPE_WARNING, '&#9785;');
	}

	public function error(string $text)
	{
		$this->viewInTag($text . ' !!!', self::NOTIFICATION_TYPE_ERROR, '&#9888;');
	}

	public function viewInTag(string $text, int $type, string $prefix = '', string $postfix = '')
	{
		if (!empty($this->classes[$type])) {
			if (!empty($prefix)) {
				$prefix = "<td>{$prefix}</td>";
			}

			if (!empty($postfix)) {
				$postfix = "<td>{$postfix}</td>";
			}

			echo "<tr style='color: {$this->classes[$type]}'>
					{$prefix}
					<td>{$text}</td>
					{$postfix}
				</tr>";
		}
	}
}