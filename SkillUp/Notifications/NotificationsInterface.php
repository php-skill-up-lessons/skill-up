<?php

namespace SkillUp\Notifications;

interface NotificationsInterface
{
	public function message(string $text);

	public function success(string $text);

	public function info(string $text);

	public function notice(string $text);

	public function warning(string $text);

	public function error(string $text);
}