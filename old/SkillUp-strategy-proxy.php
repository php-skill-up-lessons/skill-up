<?php
/*
	Необходимо доработать класс рассылки Newsletter, что бы он отправлял письма
	и пуш нотификации для юзеров из UserRepository.

	За отправку имейла мы считаем вывод в консоль строки: "Email {email} has been sent to user {name}"
	За отправку пуш нотификации: "Push notification has been sent to user {name} with device_id {device_id}"

	Так же необходимо реализовать функциональность для валидации имейлов/пушей:
	1) Нельзя отправлять письма юзерам с невалидными имейлами
	2) Нельзя отправлять пуши юзерам с невалидными device_id. Правила валидации можете придумать сами.
	3) Ничего не отправляем юзерам у которых нет имен
	4) На одно и то же мыло/device_id - можно отправить письмо/пуш только один раз

	Для обеспечения возможности масштабирования системы (добавление новых типов отправок и новых валидаторов),
	можно добавлять и использовать новые классы и другие языковые конструкции php в любом количестве
*/
//interface NewsletterSenderBase
//{
//	public function setSenderField(string $userSenderField): void;
//
//	public function getSenderField(): string;
//
//	public function send(array $userData): bool;
//
//	public function isValidSendParams(array $userData): bool;
//
//	public function alreadySentMessage(string $senderId): string;
//}
//
//abstract class NewsletterSenderBase implements NewsletterSender
//// OR
abstract class NewsletterSenderBase
{
	public string $userSenderField;

	abstract public function send(array $userData): bool;

	abstract public function isValidSendParams(array $userData): bool;

	abstract public function alreadySentMessage(string $senderId): string;
}

class NewsletterSenderBaseChecker extends NewsletterSenderBase
{
	private NewsletterSenderBase $newsletterSender;
	private array $userSentIds = [];

	public function __construct(NewsletterSenderBase $newsletterSender)
	{
		$this->newsletterSender = $newsletterSender;
		$this->userSentIds = [];
	}

	public function send(array $userData): bool
	{
		if (!$this->isValidSendParams($userData)) {
			return false;
		}

		$senderId = $userData[$this->newsletterSender->userSenderField];

		if ($this->isSent($senderId)) {
			echo $this->newsletterSender->alreadySentMessage($senderId);

			return false;
		}

		if ($this->newsletterSender->send($userData)) {
			$this->saveSendId($senderId);

			return true;
		}

		return false;
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function isValidSendParams(array $userData): bool
	{
		if (empty($userData[$this->newsletterSender->userSenderField])) {
			echo "User {$userData['name']} doesn't have Sender field '{$this->newsletterSender->userSenderField}' !\n";

			return false;
		}

		return true;
	}

	/**
	 * Check is been send by email
	 *
	 * @param string $userSendId
	 *
	 * @return bool
	 */
	public function isSent(string $userSendId): bool
	{
		if (!isset($this->userSentIds)) {
			$this->userSentIds = [];

			return false;
		}

		return in_array($userSendId, $this->userSentIds);
	}

	public function alreadySentMessage(string $senderId): string
	{
		return "Message by '{$senderId}' has already been sent !\n";
	}

	/**
	 * Saving sent Ids
	 *
	 * @param string $userSendId
	 */
	public function saveSendId(string $userSendId): void
	{
		$this->userSentIds[] = $userSendId;
	}
}

class NewsletterSenderEmail extends NewsletterSenderBase
{
	public function __construct()
	{
		$this->userSenderField = 'email';
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function send(array $userData): bool
	{
		if (!$this->isValidSendParams($userData)) {
			return false;
		}

		echo "Email '{$userData[$this->userSenderField]}' has been sent to user {$userData['name']}\n";

		return true;
	}

	public function isValidSendParams(array $userData): bool
	{
		if (!filter_var($userData[$this->userSenderField], FILTER_VALIDATE_EMAIL)) {
			echo "Email is not valid!\n";

			return false;
		}

		return true;
	}

	public function alreadySentMessage(string $senderId): string
	{
		return "Email '{$senderId}' has already been sent !\n";
	}
}

class NewsletterSenderPush extends NewsletterSenderBase
{
	public function __construct()
	{
		$this->userSenderField = 'device_id';
	}

	public function send(array $userData): bool
	{
		if (!$this->isValidSendParams($userData)) {
			return false;
		}

		echo "Push notification has been sent to user {$userData['name']} with device_id '{$userData[$this->userSenderField]}'\n";

		return true;
	}

	public function isValidSendParams(array $userData): bool
	{
		$deviceRegex = '/^\w+$/u';

		if (!preg_match($deviceRegex, $userData[$this->userSenderField])) {
			echo "Push device is not valid!\n";

			return false;
		}

		return true;
	}

	public function alreadySentMessage(string $senderId): string
	{
		return "Push notification has already been sent with device_id '{$senderId}' !\n";
	}
}

class Newsletter
{
	private array $senderClassesExisting = [];

	/**
	 * @param array $userData
	 */
	public function startSend(array $userData): void
	{
		if (empty($userData['name'])) {
			echo "\tUser doesn't have Name !..\n";

			return;
		} else {
			echo "\tUser {$userData['name']} ...\n";
		}

		$senderClasses = $this->getSenderClasses($userData);

		foreach ($senderClasses as $senderType => $senderClass) {
			if (empty($this->senderClassesExisting[$senderType])) {
				$this->senderClassesExisting[$senderType] = new NewsletterSenderBaseChecker(new $senderClass());
			}

			$this->senderClassesExisting[$senderType]->send($userData);
		}

	}

	public function getSenderClasses(array $userData): array
	{
		$userDataFull = $userData;
		unset($userData['name']);
		$userKeys = array_keys($userData);
		$senderClasses = [];	// [senderType => realClassName]

		foreach ($userKeys as $userKey) {
			switch ($userKey) {
				case 'email':
					$senderClasses['email'] = 'NewsletterSenderEmail';
					break;
				case 'device_id':
					$senderClasses['push'] = 'NewsletterSenderPush';
					break;
				default:
					break;
			}
		}

		if (empty($senderClasses)) {
			echo "User {$userDataFull['name']} doesn't have a supported subscriber distribution identifier!\n";
		}

		return $senderClasses;
	}
}

class UserRepository
{
	public static function getUsers(): array
	{
		return [
			[
				'name' => 'Ivan',
				'email' => 'ivan@test.com',
				'device_id' => 'Ks[dqweer4'
			],
			[
				'name' => 'Peter',
				'email' => 'peter@test.com'
			],
			[
				'name' => 'Mark',
				'device_id' => 'Ks[dqweer4'
			],
			[
				'name' => 'Nina',
				'email' => '...'
			],
			[
				'name' => 'Luke',
				'device_id' => 'vfehlfg43g'
			],
			[
				'name' => 'Zerg',
				'device_id' => ''
			],
			[
				'email' => '...',
				'device_id' => ''
			],
			[
				'name' => 'Peter2',
				'email' => 'peter@test.com'
			],
			[
				'name' => 'Luke2',
				'device_id' => 'vfehlfg43g'
			],
			[
				'name' => '',
				'email' => 'ivanovw@test.com',
				'device_id' => 'gv754vqnfv'
			],
		];
	}
}

/**
Тут релизовать получение объекта(ов) рассылки Newsletter и вызов(ы) метода send()
$newsletter = //... TODO
$newsletter->send();
...
 */

$newsletter = new Newsletter();
$users = UserRepository::getUsers();

foreach ($users as $number => $user) {
	$newsletter->startSend($user);
}
