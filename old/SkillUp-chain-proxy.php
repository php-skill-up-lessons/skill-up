<?php
/*
	Необходимо доработать класс рассылки Newsletter, что бы он отправлял письма
	и пуш нотификации для юзеров из UserRepository.

	За отправку имейла мы считаем вывод в консоль строки: "Email {email} has been sent to user {name}"
	За отправку пуш нотификации: "Push notification has been sent to user {name} with device_id {device_id}"

	Так же необходимо реализовать функциональность для валидации имейлов/пушей:
	1) Нельзя отправлять письма юзерам с невалидными имейлами
	2) Нельзя отправлять пуши юзерам с невалидными device_id. Правила валидации можете придумать сами.
	3) Ничего не отправляем юзерам у которых нет имен
	4) На одно и то же мыло/device_id - можно отправить письмо/пуш только один раз

	Для обеспечения возможности масштабирования системы (добавление новых типов отправок и новых валидаторов),
	можно добавлять и использовать новые классы и другие языковые конструкции php в любом количестве
*/
interface NewsletterSender
{
	public function send(array $userData): bool;

	public function isValidSendParams(array $userData): bool;

	public function alreadySentMessage(string $senderId): string;
}

abstract class NewsletterSenderMiddleware implements NewsletterSender
{
	private NewsletterSenderMiddleware $next;

	public string $userSenderField;

	/**
	 * This method can be used to build a chain of sender objects.
	 *
	 * @param \NewsletterSenderMiddleware $next
	 *
	 * @return \NewsletterSenderMiddleware
	 */
	public function addNextSender(NewsletterSenderMiddleware $next): NewsletterSenderMiddleware
	{
		$this->next = new NewsletterSenderBaseProxy($next);

		return $next;
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function send(array $userData): bool
	{
		if (!isset($this->next) || !$this->next) {
			return true;
		}

		return $this->next->send($userData);
	}
}

class NewsletterSenderBaseProxy extends NewsletterSenderMiddleware
{
	private NewsletterSenderMiddleware $newsletterSenderMiddleware;
	private array $userSentIds = [];

	public function __construct(NewsletterSenderMiddleware $newsletterSenderMiddleware)
	{
		$this->newsletterSenderMiddleware = $newsletterSenderMiddleware;
	}

	public function send(array $userData): bool
	{
		if (!$this->isValidSendParams($userData)) {
			return $this->getSentNext($userData);
		}

		$senderId = $userData[$this->newsletterSenderMiddleware->userSenderField];

		if ($this->isSent($senderId)) {
			echo $this->newsletterSenderMiddleware->alreadySentMessage($senderId);

			return $this->getSentNext($userData);
		}

		if ($this->newsletterSenderMiddleware->send($userData)) {
			$this->saveSendId($senderId);
		}

		return $this->getSentNext($userData);
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function isValidSendParams(array $userData): bool
	{
		if (empty($userData[$this->newsletterSenderMiddleware->userSenderField])) {
			echo "User {$userData['name']} doesn't have Sender field '{$this->newsletterSenderMiddleware->userSenderField}' !\n";

			return false;
		}

		return true;
	}

	function getSentNext($userData): bool
	{
		return parent::send($userData);
	}

	/**
	 * Check is been send by email
	 *
	 * @param string $userSendId
	 *
	 * @return bool
	 */
	public function isSent(string $userSendId): bool
	{
		if (!isset($this->userSentIds)) {
			$this->userSentIds = [];

			return false;
		}

		return in_array($userSendId, $this->userSentIds);
	}

	public function alreadySentMessage(string $senderId): string
	{
		return "Message by '{$senderId}' has already been sent !\n";
	}

	/**
	 * Saving sent Ids
	 *
	 * @param string $userSendId
	 */
	public function saveSendId(string $userSendId): void
	{
		$this->userSentIds[] = $userSendId;
	}
}

class NewsletterSenderEmail extends NewsletterSenderMiddleware
{
	public function __construct()
	{
		$this->userSenderField = 'email';
	}

	/**
	 * @param array $userData
	 *
	 * @return bool
	 */
	public function send(array $userData): bool
	{
		if (!$this->isValidSendParams($userData)) {
			return false;
		}

		echo "Email '{$userData[$this->userSenderField]}' has been sent to user {$userData['name']}\n";

		return true;
	}

	public function isValidSendParams(array $userData): bool
	{
		if (!filter_var($userData[$this->userSenderField], FILTER_VALIDATE_EMAIL)) {
			echo "Email is not valid!\n";

			return false;
		}

		return true;
	}

	public function alreadySentMessage(string $senderId): string
	{
		return "Email '{$senderId}' has already been sent !\n";
	}
}

class NewsletterSenderPush extends NewsletterSenderMiddleware
{
	public function __construct()
	{
		$this->userSenderField = 'device_id';
	}

	public function send(array $userData): bool
	{
		if (!$this->isValidSendParams($userData)) {
			return false;
		}

		echo "Push notification has been sent to user {$userData['name']} with device_id '{$userData[$this->userSenderField]}'\n";

		return true;
	}

	public function isValidSendParams(array $userData): bool
	{
		$deviceRegex = '/^\w+$/u';

		if (!preg_match($deviceRegex, $userData[$this->userSenderField])) {
			echo "Push device is not valid!\n";

			return false;
		}

		return true;
	}

	public function alreadySentMessage(string $senderId): string
	{
		return "Push notification has already been sent with device_id '{$senderId}' !\n";
	}
}

class Newsletter
{
	/**
	 * @var \NewsletterSenderMiddleware
	 */
	private NewsletterSenderMiddleware $senders;

	/**
	 * You can configure the Newsletter with a chain of sender objects.
	 *
	 * @param \NewsletterSenderMiddleware $senders
	 */
	public function setSenders(NewsletterSenderMiddleware $senders): void
	{
		$this->senders = $senders;
	}

	public function startSend(array $userData): void
	{
		if (empty($userData['name'])) {
			echo "\tUser doesn't have Name !..\n";

			return;
		} else {
			echo "\tUser {$userData['name']} ...\n";
		}

		$this->senders->send($userData);
	}
}

class UserRepository
{
	public static function getUsers(): array
	{
		return [
			[
				'name' => 'Ivan',
				'email' => 'ivan@test.com',
				'device_id' => 'Ks[dqweer4'
			],
			[
				'name' => 'Peter',
				'email' => 'peter@test.com'
			],
			[
				'name' => 'Mark',
				'device_id' => 'Ks[dqweer4'
			],
			[
				'name' => 'Nina',
				'email' => '...'
			],
			[
				'name' => 'Luke',
				'device_id' => 'vfehlfg43g'
			],
			[
				'name' => 'Zerg',
				'device_id' => ''
			],
			[
				'email' => '...',
				'device_id' => ''
			],
			[
				'name' => 'Peter2',
				'email' => 'peter@test.com'
			],
			[
				'name' => 'Luke2',
				'device_id' => 'vfehlfg43g'
			],
			[
				'name' => '',
				'email' => 'ivanovw@test.com',
				'device_id' => 'gv754vqnfv'
			],
		];
	}
}

/**
Тут релизовать получение объекта(ов) рассылки Newsletter и вызов(ы) метода send()
$newsletter = //... TODO
$newsletter->send();
...
 */

$senders = new NewsletterSenderBaseProxy(new NewsletterSenderEmail());
$senders->addNextSender(new NewsletterSenderPush());

$newsletter = new Newsletter();
$newsletter->setSenders($senders);

$users = UserRepository::getUsers();

foreach ($users as $number => $user) {
	$newsletter->startSend($user);
}
